/*
Copyright © 2022 XPOL <paolofalomo@gmail.com>
License Restrictions
*/
package cmd

import (
	"bufio"
	"fmt"
	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/vg"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"

	"github.com/spf13/cobra"
)

type LogData struct {
	CCD     int
	NFrame  int
	MJD     float64
	MJDok   bool
	Exptim  float64
	Mfwhm   float64
	Mbeta   float64
	X       float64
	Xe      float64
	Y       float64
	Ye      float64
	Fwhm    float64
	Fwhme   float64
	Beta    float64
	Betae   float64
	Counts  float64
	Countse float64
	Sky     float64
	Skye    float64
	Nsky    int
	Nrej    int
	Cmax    int
	Flag    int
}

//TODO extract
//CCD
//nframe
//MJD
//counts

//TODO group by ccd

//TODO for each group find medium, median, min and max of counts of each group

//TODO for each group plot x mjd and y is counts

// TODO for each group show in the plot previously found medium, median, min and
var inputFile string // Variable to hold input file name
var limit int

var extractorCmd = &cobra.Command{
	Use:   "extractor",
	Short: "A brief description of your command",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("extractor called")

		if inputFile == "" {
			fmt.Println("Please provide an input file")
			return
		}

		// Read the input log file
		file, err := os.Open(inputFile)
		if err != nil {
			log.Fatalf("Error opening the file: %s", err)
		}
		defer file.Close()

		// Slice to hold parsed log data
		var logEntries []LogData

		// Read each line of the file
		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			line := scanner.Text()

			// Skip lines starting with "#"
			if strings.HasPrefix(line, "#") {
				continue
			}

			// Split the line by spaces
			fields := strings.Fields(line)

			// Map the fields to LogData struct
			var entry LogData
			entry.CCD, _ = strconv.Atoi(fields[0])               // Assuming CCD is an integer
			entry.NFrame, _ = strconv.Atoi(fields[1])            // Assuming nframe is an integer
			entry.MJD, _ = strconv.ParseFloat(fields[2], 64)     // Assuming MJD is a float
			entry.MJDok, _ = strconv.ParseBool(fields[3])        // Assuming MJDok is a boolean
			entry.Exptim, _ = strconv.ParseFloat(fields[4], 32)  // Assuming Exptim is a float
			entry.Mfwhm, _ = strconv.ParseFloat(fields[5], 32)   // Assuming mfwhm is a float
			entry.Mbeta, _ = strconv.ParseFloat(fields[6], 32)   // Assuming mbeta is a float
			entry.X, _ = strconv.ParseFloat(fields[7], 32)       // Assuming x is a float
			entry.Xe, _ = strconv.ParseFloat(fields[8], 32)      // Assuming xe is a float
			entry.Y, _ = strconv.ParseFloat(fields[9], 32)       // Assuming y is a float
			entry.Ye, _ = strconv.ParseFloat(fields[10], 32)     // Assuming ye is a float
			entry.Fwhm, _ = strconv.ParseFloat(fields[11], 32)   // Assuming fwhm is a float
			entry.Fwhme, _ = strconv.ParseFloat(fields[12], 32)  // Assuming fwhme is a float
			entry.Beta, _ = strconv.ParseFloat(fields[13], 32)   // Assuming beta is a float
			entry.Betae, _ = strconv.ParseFloat(fields[14], 32)  // Assuming betae is a float
			entry.Counts, _ = strconv.ParseFloat(fields[15], 32) // Assuming counts is a float
			// Convert other fields similarly...

			logEntries = append(logEntries, entry)
		}

		if err := scanner.Err(); err != nil {
			log.Fatalf("Error scanning the file: %s", err)
		}

		// TODO: Perform operations on logEntries as per your requirements

		// Map to store logEntries grouped by CCD
		groupedByCCD := make(map[int][]LogData)

		// Iterate through logEntries to group by CCD
		for _, entry := range logEntries {
			groupedByCCD[entry.CCD] = append(groupedByCCD[entry.CCD], entry)
		}

		// Print rows per group with limit
		for ccd, entries := range groupedByCCD {
			fmt.Printf("CCD %d:\n", ccd)
			limitCount := *&limit
			for _, entry := range entries {
				if limitCount > 0 {
					fmt.Printf("\tnframe: %d, MJD: %.2f, counts: %.2f\n", entry.NFrame, entry.MJD, entry.Counts)
					limitCount--
				} else {
					fmt.Printf("\t...\n")
					break
				}
			}
			var counts []float64 // Slice to store "counts" for calculations

			for _, entry := range entries {
				counts = append(counts, entry.Counts) // Add counts to the slice for calculations
			}

			// Calculate median, minimum, maximum, and mean of "counts"
			if len(counts) > 0 {
				sort.Float64s(counts)
				medianValue := median(counts)
				minValue := counts[0]
				maxValue := counts[len(counts)-1]

				sum := 0.0
				for _, val := range counts {
					sum += val
				}
				meanValue := sum / float64(len(counts))

				fmt.Printf("\tMedian: %.2f, Min: %.2f, Max: %.2f, Mean: %.2f\n", medianValue, minValue, maxValue, meanValue)
			}

			//PLOTTING
			p := plot.New()
			if err != nil {
				fmt.Println("Error creating plot: ", err)
				return
			}

			// Create a plotter for points
			points := make(plotter.XYs, len(entries))

			// Populate plotter points with MJD and Counts data
			for i, entry := range entries {
				points[i].X = entry.MJD
				points[i].Y = entry.Counts
			}

			// Create a scatter plot using the plotter points
			s, err := plotter.NewScatter(points)
			if err != nil {
				fmt.Println("Error creating scatter plot: ", err)
				return
			}

			// Add the scatter plot to the plot
			p.Add(s)

			// Set the plot title and axis labels
			p.Title.Text = fmt.Sprintf("CCD %d: Counts vs MJD", ccd)
			p.X.Label.Text = "MJD"
			p.Y.Label.Text = "Counts"

			// Save the plot to a file
			filename := fmt.Sprintf("CCD_%d_plot.png", ccd)
			if err := p.Save(6*vg.Inch, 4*vg.Inch, filename); err != nil {
				fmt.Println("Error saving plot: ", err)
				return
			}
		}

	},
}

func init() {
	rootCmd.AddCommand(extractorCmd)

	extractorCmd.Flags().IntVarP(&limit, "limit", "l", 5, "Limit the number of rows per group")

	// Flag to specify the input file
	extractorCmd.Flags().StringVarP(&inputFile, "input", "i", "", "Input .log file")
}

func median(data []float64) float64 {
	sort.Float64s(data)
	mid := len(data) / 2
	if len(data)%2 == 0 {
		return (data[mid-1] + data[mid]) / 2
	}
	return data[mid]
}
