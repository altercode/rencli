/*
Copyright © 2022 XPOL <paolofalomo@gmail.com>
License Restrictions
*/
package cmd

import (
	"encoding/csv"
	"fmt"
	"golang.org/x/text/encoding/unicode"
	"golang.org/x/text/transform"
	"os"
	"strings"

	"github.com/spf13/cobra"
)

var csvCmd = &cobra.Command{
	Use:   "csv [input]",
	Short: "CSV file manipulation",
	Long:  `Converts a given csv file into a txt`,
	Args:  cobra.ExactArgs(1),
	Run:   convertCSVToTXT,
}

func init() {
	rootCmd.AddCommand(csvCmd)
}

func convertCSVToTXT(cmd *cobra.Command, args []string) {
	inputFile := args[0]
	outputFile := "output.txt"

	// Open the CSV file
	csvFile, err := os.Open(inputFile)
	if err != nil {
		fmt.Println("Error opening CSV file:", err)
		return
	}
	defer csvFile.Close()

	// Convert from UTF-16 LE to UTF-8
	utf16Decoder := unicode.UTF16(unicode.LittleEndian, unicode.IgnoreBOM)
	utf8Reader := transform.NewReader(csvFile, utf16Decoder.NewDecoder())

	// Parse the CSV file
	reader := csv.NewReader(utf8Reader)
	reader.FieldsPerRecord = -1
	reader.Comma = ';'
	records, err := reader.ReadAll()
	if err != nil {
		fmt.Println("Error reading CSV file:", err)
		return
	}

	// Create or open the output TXT file with UTF-8 encoding
	txtFile, err := os.Create(outputFile)
	if err != nil {
		fmt.Println("Error creating TXT file:", err)
		return
	}
	defer txtFile.Close()

	// Convert CSV data to a formatted table and write to the TXT file
	for _, row := range records {
		// Replace "-" with space, ":" with space, and remove ","
		for i := range row {
			row[i] = strings.Replace(row[i], "-", " ", -1)
			row[i] = strings.Replace(row[i], ":", " ", -1)
			row[i] = strings.Replace(row[i], ",", "", -1)
		}
		// Format each row with columns separated by spaces
		rowStr := strings.Join(row, " ")
		_, err := fmt.Fprintln(txtFile, rowStr)
		if err != nil {
			fmt.Println("Error writing to TXT file:", err)
			return
		}
	}

	fmt.Println("Conversion successful. Output written to", outputFile)
}
