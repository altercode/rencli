/*
Copyright © 2023 XPOL <paolofalomo@gmail.com>
*/
package main

import (
	"ren/cmd"
)

func main() {
	cmd.Execute()
}
